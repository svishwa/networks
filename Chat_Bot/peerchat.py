import time
import socket
import sys
import select
avail_peers=[]
mnum=100
vl=""
message_msg=""
addr_msg=""
dst_id=""
cnt_sent=1
check_msg=0
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
def send_messages(msg,host,port):
	global sock
	sent=sock.sendto(msg,(host,port))
	data,server=sock.recvfrom(4096)
	if not data:
		return
	return data
def proto(dt):
	 global vl
	 print dt[0]
         mnumVal=dt[0][34:37]
	 pnum=str(int(dt[0][21])+1)
         src=dt[0][4:7]
	 src=src.zfill(3)
         dst=dt[0][12:15]
	 dst=dst.zfill(3)
         if dst==src_id and dt[1]!=[]:
         	msg_ack="SRC:"+dst+";DST:"+src+";PNUM:"+pnum+";HCT:1;MNUM:"+mnumVal+";VL:;MESG:ACK"
                print "sending ack",msg_ack
                sock.sendto(msg_ack,(dt[1][0],dt[1][1]))
	 else:
		if dt[1]!=[]:
			msg_ack="SRC:"+dst+";DST:"+src+";PNUM:"+pnum+";HCT:1;MNUM:"+mnumVal+";VL:;MESG:ACK"
	 		print "sending ack",msg_ack
        	 	sock.sendto(msg_ack,(dt[1][0],dt[1][1]))
		#print dt[0][26]
		hctVal=dt[0][27]
		j=dt[0].split(";")
		if hctVal==0:
			print "******************"
			print "Dropped message from"+src+" to"+dst+" - hop count exceeded"
			print j[len(j)-1]
		elif hctVal>0:
			if src_id in j[len(j)-2]:
				print "******************"
                        	print "Dropped message from"+src+" to"+dst+" - peer revisisted"
                        	print j[len(j)-1]
			else:
				print "forwarding"	
				hctVal=str(int(hctVal)-1)
				vl+=src_id+","
				for k in range(3):
					addr=avail_peers[k].split(",")
					msg_fwd="SRC:"+src+";DST:"+dst+";PNUM:"+pnum+";HCT:"+hctVal+";MNUM:"+mnumVal+";VL:"+vl+";MESG:"+j[len(j)-1]
					print "sending "+msg_fwd+" to "+addr[0]
					sock.sendto(msg_fwd,(addr[1],int(addr[2])))
			

if __name__=="__main__":
	host="steel.isi.edu"
	port=63682
	mesg="SRC:000;DST:999;PNUM:1;HCT:1;MNUM:"+str(mnum)+";VL:;MESG:register"
	print "sending",mesg," from client to server\n"
	try:
        	data=send_messages(mesg,host,port)
		data_list=data.split(";")
		msg_list=data_list[len(data_list)-1].split(" ")
		src_id=msg_list[2][:len(msg_list[2])-1]
		src_id=src_id.zfill(3)
		src_addr=src_id+","+msg_list[len(msg_list)-1][:13]+","+msg_list[len(msg_list)-1][14:]
		if(data_list[4]=="MNUM:"+str(mnum)):	
			if(msg_list[0]!="MESG:registered" or data[16:22]!="PNUM:2"):
				print msg_list
			else:
				print "Successfully registered.My ID is: ",src_id
		else:
			print "MNUM did not match"
		while True:
			#choice=raw_input("enter choice:")
			socket_list=[sys.stdin,sock]
			read_sockets, write_sockets, error_sockets = select.select(socket_list ,[],[],5.0)
		 	if not read_sockets:
				if message_msg or addr_msg:
					if check_msg==0:
						if cnt_sent<=5:
							time.sleep(5)
							sock.sendto(message_msg,addr_msg)
							cnt_sent+=1
					        else:
							print "******************************"
							print "ERROR: Gave up sending to "+dst_id+"\n**********************************"
				
			for st in read_sockets:
				if st==sock:
					dt=st.recvfrom(4096)
					if not dt:
						print "disconnected"
						sys.exit()
					else:
						if dt[0][21]=="3":
							proto(dt)
						
						elif dt[0][21]=="7":
							print "***************"
							print dt[0][:7]+" broadcasted:"
							mg=dt[0].split(";")
							print mg[len(mg)-1]
							proto(dt)
						elif dt[0][21]=="4":
							check_msg=1
							print "recieved ack for message ",dt[0]
						elif dt[0][21]=="8":
							print "received ack for broadcast ",dt[0]

				else:
					choice=sys.stdin.readline()
					#data1=send_messages("SRC:"+src_id+";DST:999;PNUM:5;HCT:1;MNUM:123;VL:;MESG:get map",host,port)
					#print data1
					if choice.startswith("ids"):
						mnum+=1
						mesg1="SRC:"+src_id+";DST:999;PNUM:5;HCT:1;MNUM:"+str(mnum)+";VL:;MESG:get map"
        					data1=send_messages(mesg1,host,port)
						msg_part=data1.split(";") 
						peer_ids=msg_part[6].split("and")
						avail_peers=peer_ids[1]
						peer_ids=peer_ids[0][9:]
						print "\n*****************\nRecently seen pairs:\n",peer_ids
						peer_ids=peer_ids.split(",")
						peer_ids=[int(i) for i in peer_ids]
						print "\nKnown address:"
						avail_peers=avail_peers.split(",")
						temp=list()
						for i in avail_peers:
							if i[:3]!=src_id:
								
								if(i[len(i)-5:].isdigit()):
                                                                        p=i[len(i)-5:]
                                                                else:
                                                                        p=i[len(i)-6:]
								print i[:3]+"   "+i[4:len(i)-6]+"       "+p
								temp.append(i[:3]+","+i[4:len(i)-6]+","+p)
						avail_peers=list(temp)
						print "*******************"
					elif choice.startswith("msg"):
						mnum+=1
						check=0
						send_msg=choice.split(" ")
						dst_id=send_msg[1]
						dst_id=dst_id.zfill(3)
						for i in avail_peers:
							if dst_id in i:
								message="SRC:"+src_id+";DST:"+dst_id+";PNUM:3;HCT:1;MNUM:"+str(mnum)+";VL:;MESG:"+send_msg[2]
								ht=i.split(",")
								message_msg=message
								addr_msg=(ht[1],int(ht[2]))
								check_msg=0
								sock.sendto(message,addr_msg)
								check=1
								break
						if check==0:
							print "not in avail peer"
							message=[]
							message.append("SRC:"+src_id+";DST:"+dst_id+";PNUM:3;HCT:9;MNUM:"+str(mnum)+";VL:"+vl+";MESG:"+send_msg[2])
						   	message.append([])						
							proto(message)
							
					elif choice.startswith("all"):
						mnum+=1
						send_mesg=choice.split(" ")
						for j in avail_peers:
							val=j.split(",")
							msg_to_send="SRC:"+src_id+";DST:"+val[0].zfill(3)+";PNUM:7;HCT:1;MNUM:"+str(mnum)+";VL:;MESG:"+send_mesg[1]
							sock.sendto(msg_to_send,(val[1],int(val[2])))
							print "broadcasting to:",msg_to_send
		
	except Exception as e:
		print e
	finally:
    		print 'closing socket'
    		sock.close()
